const express = require('express');
const winston = require('winston');
const expressWinston = require('express-winston');
const axios = require('axios');

require('axios-debug-log');

const app = express();
const port = 8080;

app.use(expressWinston.logger({
  transports: [
    new winston.transports.Console()
  ],
}));

app.get('/ping', (req, res) => res.send('Hello World!'));

app.get('/test', async (req, res, next) => {
  try {
    res.json({
      message: 'success',
      products: await getProducts(),

    });
  } catch (error) {
    console.error(error);

    res.status(500).json({
      message: error,
    });
  }
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

async function getProducts() {
  const getProductsUrl = process.env.GET_PRODUCTS_URL;

  if (!getProductsUrl) {
    throw 'GET_PRODUCTS_URL environment variable not set';
  }

  try {
    const {data} = await axios.get(getProductsUrl);

    return data;

  } catch (error) {
    throw `Failed getting products: ${error}`;
  }
}