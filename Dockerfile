FROM node:10

WORKDIR /app

RUN mkdir -p /app

COPY ./package*.json ./
COPY ./src .

RUN npm install

ENV DEBUG=axios

ENTRYPOINT [ "node", "/app/app.js" ]